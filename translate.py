#!/usr/bin/env python3

import readline
# import urllib.parse
from selenium import webdriver
from sys import argv

tr_lang = 'ne';

class ArguMenToR:
    def __init__(self, *args):
        if 'help' in ' '.join(args[0]):
            print(f'''
{argv[0]} [--lang=ne]
Choices:
        Nepali: ne
        Japanese: ja
        Chinese: zh-(TW/CN)
        English: en
        Hindi: hi
            ''')
        self.argv = list(args)[0]
        self.param = 'lang'
        self.parse()

    def parse(self):
        global tr_lang
        spaced_args = ' '.join(self.argv[1:])
        if spaced_args.startswith('-'):
            if '=' in spaced_args:
                tr_lang = spaced_args.split('=')[1]
        else:
            tr_lang = spaced_args

browser = webdriver.Firefox()
ArguMenToR(argv)

_lang_conf = {
    'nepali': 'ne',
    'english': 'en',
    'japanese': 'ja',
    'chinese-tradional': 'zh-TW',
    'chinese-simplified': 'zh-CN',
    'hindi': 'hi'
}; TRP_CONSTRUCT = "https://DOMAIN.translate.goog/PATH/?_x_tr_sl=auto&_x_tr_tl=TRL&_x_tr_hl=en-US&_x_tr_pto=wapp"
_TRP_CONSTRUCT = "https://DOMAIN.translate.goog/PATH?_x_tr_sl=auto&_x_tr_tl=TRL"
# TODO: Language selection (while keeping up Nepali as default)

# TODO: URL-parsing
url = input("Enter URL you wish to translate: ")
# _lang = _lang_conf['nepali']
_TRP_CONSTRUCT = _TRP_CONSTRUCT.replace('TRL', tr_lang)

remote_domain = url.split('://')[1].split('/')[0]
#print('-'.join(remote_domain.split('.')[1:]))
#print(remote_domain.replace('.', '-'))

remote_path = '/'.join(url.split('/')[3:])
#print(remote_path)

rDUri = remote_domain.replace('-', '--')
rDUri = rDUri.replace('.', '-')

rUrl = _TRP_CONSTRUCT.replace('DOMAIN', rDUri).replace('PATH', remote_path)
if '?' in remote_path:
    remote_path = remote_path.split('?')[0]
    remote_URL_query = ''.join(url.split('?')[1:])
    rUrl = _TRP_CONSTRUCT.replace('DOMAIN', rDUri).replace('PATH', remote_path) + '&' + remote_URL_query

# print(rUrl)

print("Executing: " + rUrl + " \n\twithin #DOM .")
browser.get(rUrl)
# browser.
browser.execute_script('''document.querySelector('iframe').style.display = "none";document.querySelector('iframe').remove()''')
