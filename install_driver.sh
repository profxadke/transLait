#!/bin/bash

DRIVER_NAME="geckodriver"
LATEST_RELEASE_TAG="$(curl -s 'https://api.github.com/repos/mozilla/geckodriver/releases/latest' -o - | grep tag_name | head -n 2 | cut -d '"' -f 4)"

function download_driver () {
	axel -an 9 "https://github.com/mozilla/$DRIVER_NAME/releases/download/$LATEST_RELEASE_TAG/$DRIVER_NAME-$LATEST_RELEASE_TAG-linux64.tar.gz"
	tar xvf "$DRIVER_NAME-$LATEST_RELEASE_TAG-linux64.tar.gz"
	rm "$DRIVER_NAME-$LATEST_RELEASE_TAG-linux64.tar.gz"
}

if [ ! -f "$DRIVER_NAME" ]; then
	download_driver
else
	LOCAL_RELEASE_TAG="v$(./geckodriver -V | head -n 1 | cut -d ' ' -f 2)"
	if [[ "$LOCAL_RELEAsE_TAG" != "$LATEST_RELEASE_TAG" ]]; then
		echo "[+] Downloading Latest $DRIVER_NAME"
		rm -rf "./$DRIVER_NAME"
		download_driver
	fi
fi
export PATH="$PWD:$PATH"

sudo cp "./$DRIVER_NAME" /usr/bin
